# Source: https://github.com/michael0liver/python-poetry-docker-example/blob/master/docker/Dockerfile

# Creating a python base with shared environment variables
FROM python:3.11.0rc2-slim as python-base

ENV PYTHONUNBUFFERED=1 \
  PYTHONDONTWRITEBYTECODE=1 \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  POETRY_HOME="/opt/poetry" \
  POETRY_VIRTUALENVS_IN_PROJECT=true \
  POETRY_NO_INTERACTION=1 \
  PYSETUP_PATH="/app" \
  VENV_PATH="/app/.venv" \
  LC_ALL="C.UTF-8" \
  LANG="C.UTF-8"

ENV PATH="$POETRY_HOME/bin:$VENV_PATH/bin:$PATH"

RUN apt-get update \
  && apt-get install --no-install-recommends -y \
  curl \
  git \
  && rm -rf /var/lib/apt/lists/*

# builder-base is used to build dependencies
FROM python-base as builder-base
RUN apt-get update \
  && apt-get install --no-install-recommends -y \
  build-essential \
  && rm -rf /var/lib/apt/lists/*

# Install Poetry - respects $POETRY_VERSION & $POETRY_HOME
ENV POETRY_VERSION=1.2.2
RUN curl -sSL https://install.python-poetry.org | python3 -

# We copy our Python requirements here to cache them
# and install only runtime deps using poetry
WORKDIR $PYSETUP_PATH
COPY ./poetry.lock ./pyproject.toml ./
# The next line is required because poetry does not want to install our deps if
# src/devtools is not detected as a package
# TODO Open an issue on poetry's github to report this
RUN mkdir -p ./src/devtools && touch ./src/devtools/setup.py
RUN poetry install --no-root --without dev,linting,testing

# 'production' stage uses the clean 'python-base' stage and copyies
# in only our runtime deps that were installed in the 'builder-base'
FROM python-base as production
ENV FASTAPI_ENV=production

COPY --from=builder-base $POETRY_HOME $POETRY_HOME
COPY --from=builder-base $VENV_PATH $VENV_PATH

COPY ./src /app
WORKDIR /app

COPY ./poetry.lock ./pyproject.toml ./
# venv already has runtime deps installed we get a quicker install
RUN . $VENV_PATH/bin/activate && poetry install --without dev,linting,testing

# Copying in our entrypoint
COPY ./infra/docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

ENTRYPOINT [ "bash", "/docker-entrypoint.sh" ]

# 'development' stage installs all dev deps and can be used to develop code.
# For example using docker-compose to mount local volume under /app
FROM python-base as development
ENV FASTAPI_ENV=development

# Copying poetry and venv into image
COPY --from=builder-base $POETRY_HOME $POETRY_HOME
COPY --from=builder-base $PYSETUP_PATH $PYSETUP_PATH

WORKDIR /app
COPY ./poetry.lock ./pyproject.toml ./
# venv already has runtime deps installed we get a quicker install for dev deps
RUN . $VENV_PATH/bin/activate && poetry install --no-root

COPY . .

# run from project dir to have our package installed in edit mode
RUN . $VENV_PATH/bin/activate && poetry install

# Copying in our entrypoint
COPY ./infra/docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

ENTRYPOINT [ "bash", "/docker-entrypoint.sh" ]
