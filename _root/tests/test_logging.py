import logging
from unittest.mock import (
    MagicMock,
)

import pytest
from pytest_mock import (
    MockerFixture,
)

from tests.utils import (
    root_package,
)
from {{ package_name }}.utils.logging import (
    LogLevel,
    init_logging,
    set_level,
)


def test_init_logging_without_loggers_raises() -> None:
    with pytest.raises(
        ValueError, match="Specify at least one root logger to initialize"
    ):
        init_logging(LogLevel.INFO)


def test_set_level(
    mocker: MockerFixture,
) -> None:
    logger_mock = MagicMock(spec=logging.Logger("logger"))
    returned_logger_mock = MagicMock(spec=logging.Logger("returned_logger"))
    get_logger_mock = mocker.patch(
        f"{root_package}.utils.logging.get_logger",
        return_value=returned_logger_mock,
    )

    level = LogLevel.WARNING
    my_logger_name = "my_logger"
    set_level(level, logger_mock, my_logger_name)

    # Logger should have been retrieved from its name
    get_logger_mock.assert_called_once_with(my_logger_name)

    # The level should have been set on both loggers
    level_code = getattr(logging, level)
    logger_mock.setLevel.assert_called_once_with(level_code)
    returned_logger_mock.setLevel.assert_called_once_with(level_code)


def test_init_logging(
    mocker: MockerFixture,
) -> None:
    logger_mock = MagicMock(spec=logging.Logger("logger"))
    returned_logger_mock = MagicMock(spec=logging.Logger("returned_logger"))
    get_logger_mock = mocker.patch(
        f"{root_package}.utils.logging.get_logger",
        return_value=returned_logger_mock,
    )
    set_level_mock = mocker.patch(
        f"{root_package}.utils.logging.set_level",
    )

    level = LogLevel.WARNING
    my_logger_name = "my_logger"
    init_logging(level, logger_mock, my_logger_name)

    # Logger should have been retrieved from its name
    get_logger_mock.assert_called_once_with(my_logger_name)

    # The level should have been set on both loggers
    set_level_mock.assert_any_call(level, logger_mock)
    set_level_mock.assert_any_call(level, returned_logger_mock)
