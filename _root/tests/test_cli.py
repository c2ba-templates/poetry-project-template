from pathlib import (
    Path,
)

from pytest_mock import (
    MockerFixture,
)
from typer.testing import (
    CliRunner,
)

from tests.utils import (
    root_package,
)
from {{ package_name }} import (
    __version__,
)
from {{ package_name }}.__main__ import (
    app,
)
from {{ package_name }}.utils.cli import (
    ENVVAR_PREFIX,
    get_envvar_name,
    get_envvar_names,
)
from {{ package_name }}.utils.logging import (
    Logger,
)

runner = CliRunner(mix_stderr=False)


def test_about() -> None:
    result = runner.invoke(app, ["about"], catch_exceptions=False)
    assert result.exit_code == 0
    assert __version__ in result.stdout
    assert "{{ project_slug }}" in result.stdout


def test_version() -> None:
    result = runner.invoke(app, ["--version"], catch_exceptions=False)
    assert result.exit_code == 0
    assert __version__ == result.stdout.strip()


def test_service(
    mocker: MockerFixture,
) -> None:
    sleep_mock = mocker.patch(f"{root_package}.__main__.sleep")
    service_done_call_count = 0
    service_done_target_call_count = 3

    def _service_done() -> bool:
        nonlocal service_done_call_count
        service_done_call_count += 1
        return service_done_call_count >= service_done_target_call_count

    mocker.patch(f"{root_package}.__main__.service_done", wraps=_service_done)
    result = runner.invoke(
        app, ["service", "--seconds-delay", "10"], catch_exceptions=False
    )

    assert result.exit_code == 0
    sleep_mock.assert_called_with(10)
    assert service_done_call_count == service_done_target_call_count


def test_foo(global_datadir: Path) -> None:
    result = runner.invoke(
        app,
        [
            "--log-level",
            "debug",
            "--example-config-path",
            f"{global_datadir}",
            "example",
            "foo",
        ],
        catch_exceptions=False,
    )
    assert result.exit_code == 0
    # todo: test that global_datadir is in result.stderr, but right now typer does not seems to capture stderr...


def test_bad_log_level_fails() -> None:
    result = runner.invoke(
        app,
        [
            "--log-level",
            "bad",
            "example",
            "foo",
        ],
    )
    assert result.exit_code != 0


def test_fixtures(
    global_datadir: Path,
    datadir: Path,
    original_datadir: Path,
    shared_datadir: Path,
    original_shared_datadir: Path,
    logger: Logger,
    session_id: str,
) -> None:
    logger.info(f"Session id: {session_id}")
    logger.info(
        f"global_datadir={global_datadir}\n"
        f"datadir={datadir}\n"
        f"original_datadir={original_datadir}\n"
        f"shared_datadir={shared_datadir}\n"
        f"original_shared_datadir={original_shared_datadir}\n"
    )


def test_get_envvar_name() -> None:
    assert (
        get_envvar_name("test setting-name", "TEST-SCOPE NAME")
        == f"{ENVVAR_PREFIX}_TEST_SCOPE_NAME_TEST_SETTING_NAME"
    )


def test_get_envvar_names() -> None:
    assert set(
        get_envvar_names("test setting-name", "TEST-SCOPE NAME", use_global=True)
    ) == {
        f"{ENVVAR_PREFIX}_TEST_SETTING_NAME",
        f"{ENVVAR_PREFIX}_TEST_SCOPE_NAME_TEST_SETTING_NAME",
    }
