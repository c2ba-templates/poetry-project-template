# {{ project_name }}

## How to setup the development environment

### With Docker and Docker Compose

- Run `docker-compose -f infra/docker-compose.yml -p time-breach-generator --profile dev up -d`
  - The first time it will build the docker image
  - It will start a development container exposing the 5678 port for debug
- Run `docker-compose -f infra/docker-compose.yml -p time-breach-generator exec dev bash` to start a bash session inside the dev container
- Inside the container you can run the python code with the debugger using `python -m debugpy --listen 0.0.0.0:5678 [--wait-for-client]` followed by python args. For examples:
  - Run the generator with `python -m debugpy --listen 0.0.0.0:5678 -m time_breach_generator [COMMAND]`
  - Run tests with `python -m debugpy --listen 0.0.0.0:5678 --wait-for-client -m pytest`
  - You can then connect with a debugguer UI
- Run `docker-compose -f infra/docker-compose.yml -p time-breach-generator --profile dev build` to rebuild the docker image
- Run `docker-compose -f infra/docker-compose.yml -p time-breach-generator --profile dev down`

### With poetry and python installed on your system

- Install `poetry` 1.2+ using the official installer as described here: https://python-poetry.org/docs/#installation
- Install a python version compatible with this project
  - The python version for the project can be found in `pyproject.toml` in section `[tool.poetry.dependencies]`
  - You can use `pyenv` or `pyenv-win` on windows to install multiple version of python on your system
    - Use `pyenv install -l` to list all available versions
    - Install a version with `pyenv install VERSION`
    - Set it locally using `pyenv local VERSION`
    - Instruct poetry to use your local version of python using `poetry env use python`
- Run `poetry install` to create the local virtual env in `./.venv`
- You can activate the virtual env with `source .venv/bin/activate` on Unix systems or `source .venv/Scripts/activate` on Windows

### Useful commands

- You can install dependencies with `poetry install` when they have changed
- You can run:
  - Tests with `poetry run pytest`
  - The linter with `poetry run flake8 src/ tests/ ./noxfile.py`
  - Code formatters with:
    - `poetry run black src/ tests/ noxfile.py`
    - `poetry run isort src/ tests/ noxfile.py`
- You can install precommit hooks with `poetry run pre-commit install`
  - Note: will not work with the docker setup
- You can also run sessions automated with Nox in isolated virtual environments using:
  - `poetry run nox -s lint`
  - `poetry run nox -s test`
  - `poetry run nox -s mypy`
  - `poetry run nox -s safety`
  - You can read `noxfile.py` for available sessions or add new one
  - The main purpose of this automation script is to run on CI environment but you can run it locally if needed
  - You can use `-r` option to re-use virtual environments managed by nox
- You can run developer tools with `poetry run devtools`
  - Fill free to implement any required tooling for this project in the `src/devtools` package
- You can get access to dev bash utilities with `source .devenv/.bash_profile`
